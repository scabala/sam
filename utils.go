package main

import (
	"bufio"
	"context"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"hash"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"sort"
	"strings"
	"time"
	// "context"
	"bytes"
	"strconv"

	"gitlab.com/scabala/sam/internal/signify"

	"github.com/Masterminds/semver/v3"
	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/google/go-containerregistry/pkg/name"
	"github.com/google/go-containerregistry/pkg/v1/remote"
	"github.com/google/go-github/v40/github"
	getter "github.com/hashicorp/go-getter/v2"
	"github.com/jedisct1/go-minisign"
	"github.com/mholt/archiver/v4"
	"github.com/rivo/tview"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

/*
Sort collection as SemVer2 version.
If any element is not SemVer2 version, skip it.
*/
func SemverSort(versions []string) ([]string, error) {
	var ret []string

	vs := make([]*semver.Version, 0)
	for _, r := range versions {
		v, err := semver.NewVersion(r)
		if err != nil {
			log.Warnf("Cannot coerce '%s' to SemVer2 version. Skipping.", r)
			continue
		}

		vs = append(vs, v)
	}

	sort.Sort(semver.Collection(vs))

	for i := len(vs) - 1; i >= 0; i-- {
		ret = append(ret, vs[i].String())
	}

	return ret, nil
}

/*
Display interactive menu with provided items.
Set original choice to 'index' item
*/
func Menu(items []string, index int) (string, error) {
	sortedItems, err := SemverSort(items)
	if err != nil {
		return "", err
	}

	app := tview.NewApplication()

	var selection string
	list := tview.NewList()
	list = list.ShowSecondaryText(false)
	list = list.AddItem("Quit", "Press to exit", 'q', func() {
		app.Stop()
	})

	helpfunc := func() {
		selection, _ = list.GetItemText(list.GetCurrentItem())
		app.Stop()
	}

	for _, item := range sortedItems {
		list = list.AddItem(item, "", 0, helpfunc)
	}
	list.SetCurrentItem(index)

	err = app.SetRoot(list, true).Run()
	if err != nil {
		return "", err
	}

	return selection, nil
}

/*
Get list of relesed versions from specified git repository.
It supports following git hosting services:
  - GitHub

Result list is sorted.
*/
type ReleaseOptions struct {
	Excludes []string
}

func GitHubReleases(user string, repo string, opts ReleaseOptions) ([]string, error) {
	var versions []string

	if len(opts.Excludes) == 0 {
		opts.Excludes = []string{"alpha", "beta", "rc"}
	}

	client := github.NewClient(nil)

	opt := &github.ListOptions{Page: 1, PerPage: 100}
	ctx := context.Background()
	releases, _, err := client.Repositories.ListReleases(ctx, user, repo, opt)

	if err != nil {
		return []string{""}, err
	}

	for _, tag := range releases {
		tagName := strings.TrimPrefix(*tag.TagName, "v")
		skip := false
		for _, exc := range opts.Excludes {
			if strings.Contains(tagName, exc) {
				skip = true
			}
		}
		if !skip {
			versions = append(versions, tagName)
		}
	}
	return versions, nil
}

func GitLabReleases(user string, repo string, opts ReleaseOptions) ([]string, error) {
	var versions []string

	if len(opts.Excludes) == 0 {
		opts.Excludes = []string{"alpha", "beta", "rc"}
	}

	gl, err := gitlab.NewClient("")
	if err != nil {
		return versions, err
	}

	releases, _, err := gl.Releases.ListReleases(user+"/"+repo, nil, nil)
	if err != nil {
		return versions, err
	}

	for _, tag := range releases {
		tagName := strings.TrimPrefix(tag.TagName, "v")
		skip := false
		for _, exc := range opts.Excludes {
			if strings.Contains(tagName, exc) {
				skip = true
			}
		}
		if !skip {
			versions = append(versions, tagName)
		}
	}

	return versions, nil
}

/*
Download file from given URL
*/
func Fetch(url string, dir string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 600*time.Second) // TODO: make timeout customizable
	defer cancel()

	httpGetter := &getter.HttpGetter{
		Netrc:                 true,
		XTerraformGetDisabled: true,
		HeadFirstTimeout:      10 * time.Second,
		ReadTimeout:           600 * time.Second,
	}

	Getters := []getter.Getter{
		&getter.GitGetter{
			Detectors: []getter.Detector{
				new(getter.GitHubDetector),
				new(getter.GitDetector),
				new(getter.BitBucketDetector),
				new(getter.GitLabDetector),
			},
		},
		new(getter.HgGetter),
		new(getter.SmbClientGetter),
		new(getter.SmbMountGetter),
		httpGetter,
		new(getter.FileGetter),
	}

	cl := getter.Client{
		Getters: Getters,
	}

	req := getter.Request{Src: fmt.Sprintf("%s?archive=false", url), Dst: dir, GetMode: getter.ModeAny}

	res, err := cl.Get(ctx, &req)
	if err != nil {
		return "", err
	}
	return res.Dst, nil
}

/*
Check if given checksum of data is correct
Gets checksum from string
*/
func ChecksumString(filepath string, checksum string, algorithm string) (bool, error) {
	if algorithm == "" {
		algorithm = "sha256"
	}

	var hasher hash.Hash

	switch algorithm {
	case "sha256":
		hasher = sha256.New()
	case "sha512":
		hasher = sha512.New()
	default:
		return false, fmt.Errorf("bad algorithm: %s", algorithm)
	}

	fileobj, err := os.Open(filepath)
	if err != nil {
		return false, err
	}
	defer fileobj.Close()

	_, err = io.Copy(hasher, fileobj)
	if err != nil {
		return false, err
	}
	value := hex.EncodeToString(hasher.Sum(nil))

	if value == checksum {
		return true, nil
	}

	return false, nil
}

/*
Check if given checksum of data is correct
Gets checksum from string
*/
func Checksum(filepath string, checksumPath string, algorithm string) (bool, error) {
	var checksum string

	checksumFile, err := os.Open(checksumPath)
	if err != nil {
		return false, err
	}
	defer checksumFile.Close()

	scanner := bufio.NewScanner(checksumFile)
	for scanner.Scan() {
		line := scanner.Text()

		if strings.HasSuffix(line, path.Base(filepath)) {
			v := strings.Split(line, " ")
			checksum = v[0]
			break
		}
	}
	err = scanner.Err()
	if err != nil {
		return false, err
	}

	valid, err := ChecksumString(filepath, checksum, algorithm)
	if err != nil {
		return false, err
	}

	return valid, nil
}

/*
Verify PGP signature of data.
It will use only that key to verify data's signature.
*/
func VerifySignaturePGP(dataPath string, signaturePath string, key []byte, signatureArmored bool) (bool, error) {
	data, err := ioutil.ReadFile(dataPath)
	if err != nil {
		return false, err
	}
	message := crypto.NewPlainMessage(data)

	signature, err := ioutil.ReadFile(signaturePath)
	if err != nil {
		return false, err
	}
	var pgpSignature *crypto.PGPSignature
	if signatureArmored {
		pgpSignature, err = crypto.NewPGPSignatureFromArmored(string(signature))
		if err != nil {
			return false, err
		}
	} else {
		pgpSignature = crypto.NewPGPSignature(signature)
	}

	publicKeyObj, err := crypto.NewKeyFromArmored(string(key))
	if err != nil {
		return false, err
	}
	signingKeyRing, err := crypto.NewKeyRing(publicKeyObj)
	if err != nil {
		return false, err
	}

	err = signingKeyRing.VerifyDetached(message, pgpSignature, crypto.GetUnixTime())
	if err != nil {
		return false, err
	}
	return true, nil
}

/*
Verify cosign singature
*/
func VerifySignatureCosign(dataPath string, signaturePath string, key []byte) (bool, error) {
	return false, nil
}

/*
Verify BSD's signify signature of data.
*/
func VerifySignatureSignify(dataPath string, signaturePath string, key []byte) (bool, error) {
	data, err := ioutil.ReadFile(dataPath)
	if err != nil {
		return false, err
	}

	_, pkBytes, err := signify.ReadFile(bytes.NewReader(key))
	if err != nil {
		return false, err
	}
	pk, err := signify.ParsePublicKey(pkBytes)
	if err != nil {
		return false, err
	}

	sigReader, err := os.Open(signaturePath)
	if err != nil {
		return false, err
	}
	defer sigReader.Close()

	_, sigBytes, err := signify.ReadFile(sigReader)
	if err != nil {
		return false, err
	}
	sig, err := signify.ParseSignature(sigBytes)
	if err != nil {
		return false, err
	}

	valid := signify.Verify(pk, data, sig)
	return valid, nil
}

/*
Verify minisign signature of data.
*/
func VerifySignatureMinisign(dataPath string, signaturePath string, key []byte) (bool, error) {
	data, err := ioutil.ReadFile(dataPath)
	if err != nil {
		return false, err
	}

	pk, err := minisign.DecodePublicKey(string(key))
	if err != nil {
		return false, err
	}

	sig, err := minisign.NewSignatureFromFile(signaturePath)
	if err != nil {
		return false, err
	}

	valid, err := pk.Verify(data, sig)
	if err != nil {
		return false, err
	}
	return valid, nil
}

/*
Unpack one file 'member' from archive into 'target' path.
It supports following archive types:
  - zip
  - tar
*/
func Unpack(filepath string, member string, target string, mode string) error {
	modeInt, err := strconv.ParseInt(mode, 8, 32)
	if err != nil {
		return err
	}

	fsys, err := archiver.FileSystem(filepath)
	if err != nil {
		return err
	}

	memberFile, err := fsys.Open(member)
	if err != nil {
		return err
	}
	defer memberFile.Close()

	dst, err := os.OpenFile(target, os.O_WRONLY|os.O_CREATE, os.FileMode(int(modeInt)))
	if err != nil {
		return err
	}
	defer dst.Close()

	_, err = io.Copy(dst, memberFile)
	if err != nil {
		return err
	}
	return nil
}

/*
Copy file from src to dst and set proper mode
*/
func Copy(srcPath string, dstPath string, mode string) error {
	modeInt, err := strconv.ParseInt(mode, 8, 32)
	if err != nil {
		return err
	}

	src, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer src.Close()

	dst, err := os.OpenFile(dstPath, os.O_WRONLY|os.O_CREATE, os.FileMode(int(modeInt)))
	if err != nil {
		return err
	}
	defer dst.Close()

	_, err = io.Copy(dst, src)
	if err != nil {
		return err
	}
	return nil
}

/*
Check if version satisfies given constraint
*/
func SemVerCompare(version string, constraint string) (bool, error) {
	c, err := semver.NewConstraint(constraint)
	if err != nil {
		return false, err
	}

	v, err := semver.NewVersion(version)
	if err != nil {
		return false, err
	}
	ret := c.Check(v)
	return ret, nil
}

/*
Create symlink even when 'dst' already exists
*/
func Symlink(src string, dst string, mode string) error {
	exists, err := isFileExist(dst)
	if err != nil {
		return err
	}
	if exists {
		log.Tracef("Removing %s", dst)
		os.Remove(dst)
	}
	log.Tracef("Creating symlink: %s -> %s", src, dst)
	err = os.Symlink(src, dst)
	if err != nil {
		return err
	}
	return nil
}

/*
Remove file only when it is symlink
*/
func Unlink(path string) error {
	st, err := os.Lstat(path)
	if err != nil && os.IsNotExist(err) {
		log.Debugf("Symlink '%v' does not exist. Skipping.", path)
		return nil
	} else if err != nil {
		return err
	}
	if st.Mode()&os.ModeSymlink != 0 {
		log.Tracef("Removing symlink: %s", path)
		err = os.Remove(path)
		if err != nil {
			return err
		}
		return nil
	}

	return errors.New(path + " is not symlink")
}

/*
Get tags of image from OCI container registry
*/
func ContainerRegistryTags(image string, opts ReleaseOptions) ([]string, error) {
	var versions []string

	if len(opts.Excludes) == 0 {
		opts.Excludes = []string{"alpha", "beta", "rc"}
	}

	rep, err := name.NewRepository(image)
	if err != nil {
		return []string{""}, err
	}

	tags, err := remote.List(rep)
	if err != nil {
		return []string{""}, err
	}

	for _, tag := range tags {
		tagName := strings.TrimPrefix(tag, "v")
		skip := false
		for _, exc := range opts.Excludes {
			if strings.Contains(tagName, exc) {
				skip = true
			}
		}
		if !skip {
			versions = append(versions, tagName)
		}
	}
	return versions, nil
}

type pypiReleases struct {
	Releases []string `json:"releases"`
}

/*
Get tags of image from OCI container registry
*/
func PypiReleases(pkg string, opts ReleaseOptions) ([]string, error) {
	var versions []string

	if len(opts.Excludes) == 0 {
		opts.Excludes = []string{"alpha", "beta", "rc"}
	}

	resp, err := http.Get("https://pypi.python.org/pypi/" + pkg + "/json")
	if err != nil {
		return []string{""}, err
	}
	defer resp.Body.Close()

	var p pypiReleases

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&p)
	if err != nil {
		return []string{""}, err
	}

	for _, tag := range p.Releases {
		tagName := strings.TrimPrefix(tag, "v")
		skip := false
		for _, exc := range opts.Excludes {
			if strings.Contains(tagName, exc) {
				skip = true
			}
		}
		if !skip {
			versions = append(versions, tagName)
		}
	}
	return versions, nil
}
