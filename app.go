package main

//TODO: this file is really messy. Refactor it.

import (
	"encoding/json"
	"fmt"
	"os"
	"path"

	log "github.com/sirupsen/logrus"
	"path/filepath"
)

type Application struct {
	installRoot    string
	pluginRegistry *PluginRegistry
	cache          *Cache
}

func NewApp(logLevel string) (*Application, error) {
	switch logLevel {
	case "trace":
		log.SetLevel(log.TraceLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	case "warning":
		log.SetLevel(log.WarnLevel)
	}

	log.SetFormatter(&log.TextFormatter{
		FullTimestamp: true,
	})

	app := Application{}

	value := os.Getenv("SAM_INSTALL_ROOT")
	if value == "" {
		value = os.ExpandEnv("${HOME}/.local/share/sam")
	}
	app.installRoot = value
	log.Debugf("InstallRoot: %s", app.installRoot)

	// get plugin root

	app.pluginRegistry = app.getPluginRegistry()

	err := app.pluginRegistry.loadPlugins()
	if err != nil {
		return nil, err
	}

	c, err := app.getDiskCache()
	if err != nil {
		return nil, err
	}
	app.cache = c

	return &app, nil
}

func (a *Application) getPluginRegistry() *PluginRegistry {
	sam_plugin_root := os.Getenv("SAM_PLUGIN_ROOT")
	if sam_plugin_root == "" {
		xdg_data_home := os.Getenv("XDG_DATA_HOME")
		if xdg_data_home == "" {
			xdg_data_home = filepath.Join(os.Getenv("HOME"), ".local", "share")
		}

		sam_plugin_root = filepath.Join(xdg_data_home, "sam", "plugins")
	}
	log.Debugf("PluginRoot: %s", sam_plugin_root)

	return &PluginRegistry{pluginRoot: sam_plugin_root, plugins: make(map[string]Plugin)}
}

func (a *Application) getDiskCache() (*Cache, error) {
	cachePath := os.Getenv("SAM_CACHE")
	if cachePath == "" {

		xdgCache := os.Getenv("XDG_CACHE_HOME")
		if xdgCache == "" {
			xdgCache = os.ExpandEnv("${HOME}/.cache")
		}

		cachePath = path.Join(xdgCache, "sam.json")
	}

	return NewCache(cachePath, 1)
}

func (a *Application) isElementExist(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

func isFileExist(s string) (bool, error) {
	_, err := os.Stat(s)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		} else {
			return false, err
		}
	}
	return true, nil
}

func (a *Application) isPresent(name string, version string) (bool, error) {
	p := path.Join(a.installRoot, name, version)
	return isFileExist(p)
}

func (a *Application) DoInstall(pluginName string, release string) error {
	plugin, err := a.pluginRegistry.get(pluginName)
	if err != nil {
		return err
	}

	// get exact version
	version, err := a.getVersion(plugin, release)
	if err != nil {
		return err
	}

	pm, err := NewPluginMetadata(path.Join(a.installRoot, plugin.Name()))
	if err != nil {
		return err
	}

	md, err := pm.Get()
	if err != nil {
		return err
	}
	if md.InstalledVersion == version {
		return nil
	}

	// if archive not exists, execute function Download
	exists, err := a.isPresent(plugin.Name(), version)
	if err != nil {
		return err
	}
	if !exists {
		log.Infof("%s %s not present, downloading...", plugin.Name(), version)
		err = a.download(plugin, version)
		if err != nil {
			return err
		}
	}

	err = a.uninstall(plugin)
	if err != nil {
		return err
	}
	err = a.install(plugin, version)
	if err != nil {
		return err
	}

	return nil
}

func (a *Application) DoUninstall(pluginName string) error {
	plugin, err := a.pluginRegistry.get(pluginName)
	if err != nil {
		return err
	}

	pm, err := NewPluginMetadata(path.Join(a.installRoot, plugin.Name()))
	if err != nil {
		return err
	}

	md, err := pm.Get()
	if err != nil {
		return err
	}
	if md.InstalledVersion == "" {
		return nil
	}

	err = a.uninstall(plugin)
	if err != nil {
		return err
	}

	return nil
}

func (a *Application) DoGetVersions(pluginName string) ([]string, error) {
	plugin, err := a.pluginRegistry.get(pluginName)
	if err != nil {
		return []string{""}, err
	}

	vers, err := a.cachedVersions(plugin)
	if err != nil {
		return []string{""}, err
	}

	sortedVersions, err := SemverSort(vers)
	if err != nil {
		return []string{""}, err
	}

	return sortedVersions, nil
}

func (a *Application) DoPurge(pluginName string) error {
	plugin, err := a.pluginRegistry.get(pluginName)
	if err != nil {
		return err
	}

	err = a.uninstall(plugin)
	if err != nil {
		return err
	}

	err = a.purge(plugin)
	if err != nil {
		return err
	}

	return nil
}

func (a *Application) getVersion(plugin Plugin, release string) (string, error) {
	unsortedVersions, err := a.cachedVersions(plugin)
	if err != nil {
		return "", err
	}

	versions, err := SemverSort(unsortedVersions)
	if err != nil {
		return "", err
	}

	var version string

	if release != "" {
		log.Tracef("Release provided: %s", release)
		if release == "latest" {
			version = versions[0]
		} else if a.isElementExist(versions, release) {
			version = release
		} else {
			return "", fmt.Errorf("%s: no such version", release)
		}
	} else {
		log.Trace("No release provided, will display interactive menu")
		version, err = Menu(versions, 0)
		if err != nil {
			return "", err
		}
		log.Tracef("Release chosen from menu: %s", version)

		if version == "" {
			os.Exit(0)
		}
	}
	log.Tracef("Release to use: %s", version)
	return version, nil
}

func (a *Application) download(plugin Plugin, release string) error {
	downloadDir := path.Join(a.installRoot, plugin.Name())
	log.Tracef("DownlodDir: %s", downloadDir)
	versionPath := path.Join(downloadDir, release)
	log.Tracef("VersionPath: %s", versionPath)

	err := os.MkdirAll(versionPath, 0700)
	if err != nil {
		return err
	}
	log.Tracef("Calling function 'download' from plugin %s", plugin.Name())
	err = plugin.Download(release, versionPath+"/")
	if err != nil {
		log.Errorf("Caught error in 'download' function: %v", err)
		return nil
	}

	log.Tracef("Function 'download' from plugin %s returned", plugin.Name())

	return nil
}

type PluginMetadata struct {
	InstalledVersion string `json:"installed_version"`
	metadataPath     string
}

func NewPluginMetadata(pluginPath string) (*PluginMetadata, error) {
	pm := PluginMetadata{}

	metadataPath := path.Join(pluginPath, "metadata.json")
	log.Tracef("PluginMetadata path: %s", metadataPath)

	pm.metadataPath = metadataPath

	_, err := os.ReadFile(metadataPath)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(path.Dir(metadataPath), 0700)
			if err != nil {
				return nil, err
			}
			err = os.WriteFile(metadataPath, []byte("{}"), os.ModePerm)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}

	return &pm, nil
}

func (pm *PluginMetadata) Set() error {
	var md PluginMetadata

	metadataStr, err := os.ReadFile(pm.metadataPath)
	if err != nil {
		return err
	}

	err = json.Unmarshal(metadataStr, &md)
	if err != nil {
		return err
	}

	md.InstalledVersion = pm.InstalledVersion

	newMetadataStr, err := json.Marshal(md)
	if err != nil {
		return err
	}
	err = os.WriteFile(pm.metadataPath, newMetadataStr, os.ModePerm)
	if err != nil {
		return err
	}

	return nil
}

func (pm *PluginMetadata) Get() (*PluginMetadata, error) {
	var md PluginMetadata

	metadataStr, err := os.ReadFile(pm.metadataPath)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(metadataStr, &md)
	if err != nil {
		return nil, err
	}

	return &md, nil
}

func (a *Application) install(plugin Plugin, release string) error {
	versionPath := path.Join(a.installRoot, plugin.Name(), release) + "/"

	log.Tracef("VersionPath: %s", versionPath)

	log.Tracef("Calling function 'install' from plugin %s", plugin.Name())
	err := plugin.Install(versionPath)
	if err != nil {
		log.Errorf("Caught error in 'install' function: %v", err)
		return nil
	}
	log.Tracef("Function 'install' from plugin %s returned", plugin.Name())

	pm, err := NewPluginMetadata(path.Join(a.installRoot, plugin.Name()))
	if err != nil {
		return err
	}
	pm.InstalledVersion = release
	err = pm.Set()
	if err != nil {
		return err
	}
	log.Infof("Using %s %s", plugin.Name(), release)

	return nil
}

func (a *Application) uninstall(plugin Plugin) error {
	log.Tracef("Calling function 'uninstall' from plugin %s", plugin.Name())
	err := plugin.Uninstall()
	if err != nil {
		log.Errorf("Caught error in 'uninstall' function: %v", err)
		return nil
	}
	log.Tracef("Function 'uninstall' from plugin %s returned", plugin.Name())

	pm, err := NewPluginMetadata(path.Join(a.installRoot, plugin.Name()))
	if err != nil {
		return err
	}
	pm.InstalledVersion = ""
	err = pm.Set()
	if err != nil {
		return err
	}
	return nil
}

func (a *Application) purge(plugin Plugin) error {
	p := path.Join(a.installRoot, plugin.Name())
	log.Tracef("Removing %s", p)
	err := os.RemoveAll(p)
	if err != nil {
		return err
	}
	return nil
}

func (a *Application) DoCurrent(pluginName string) (string, error) {
	pm, err := NewPluginMetadata(path.Join(a.installRoot, pluginName))
	if err != nil {
		return "", err
	}

	md, err := pm.Get()
	if err != nil {
		return "", err
	}
	return md.InstalledVersion, nil
}

func (a *Application) cachedVersions(plugin Plugin) ([]string, error) {
	versions, err := a.cache.Get(plugin.Name())
	if err != nil {
		log.Debug("No versions in cache, getting fresh data")
		versions, err = plugin.Versions()
		if err != nil {
			log.Errorf("Caught error in 'versions' function: %v", err)
			return []string{""}, nil
		}
		err = a.cache.Set(plugin.Name(), versions)
		if err != nil {
			log.Errorf("Cannot set cache for %s. Reason: %v", plugin.Name(), err)
		}
	}
	return versions, nil
}

func (a *Application) DoCheckUpdate(pluginName string) (string, error) {
	plugin, err := a.pluginRegistry.get(pluginName)
	if err != nil {
		return "", err
	}

	pm, err := NewPluginMetadata(path.Join(a.installRoot, plugin.Name()))
	if err != nil {
		return "", err
	}

	md, err := pm.Get()
	if err != nil {
		return "", err
	}

	versions, err := a.cachedVersions(plugin)
	if err != nil {
		return "", err
	}

	sortedVersions, err := SemverSort(versions)
	if err != nil {
		return "", err
	}

	if md.InstalledVersion != sortedVersions[0] {
		return sortedVersions[0], nil
	}

	return "", nil
}

func (a *Application) DoCheckUpdateAll() (map[string]string, error) {
	updates := make(map[string]string)
	for _, p := range a.pluginRegistry.getAll() {

		update, err := a.DoCheckUpdate(p.Name())
		if err != nil {
			return updates, err
		}
		if update != "" {
			updates[p.Name()] = update
		}
	}

	return updates, nil
}

func (a *Application) DoCurrentAll() (map[string]string, error) {
	currents := make(map[string]string)
	for _, p := range a.pluginRegistry.getAll() {

		current, err := a.DoCurrent(p.Name())
		if err != nil {
			return currents, err
		}
		currents[p.Name()] = current
	}

	return currents, nil
}

func (a *Application) DoUpdateAll() error {
	for _, p := range a.pluginRegistry.getAll() {

		err := a.DoInstall(p.Name(), "latest")
		if err != nil {
			return err
		}
	}

	return nil
}

func (a *Application) DoGetPluginNames() []string {
	var names []string

	for _, p := range a.pluginRegistry.getAll() {
		names = append(names, p.Name())
	}

	return names
}
