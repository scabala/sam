# sam

Simple Go app to maintain multiple versions of statically compiled console applications

## Installation

`sam` is just Go application and can be installed in any way.

### From source

Easiest way is to compile it from source.

Build requirements:
 - go 1.17 and up
 - [just](https://github.com/casey/just)

Clone this repo, enter repository directory and run `just`.  
It will build statically linked application to file `sam`.  
You can place this binary in your `PATH` or wherever you want.  

### From binary

There are pre-built binaries for various platforms. To install one of those, you will need:

 - tar
 - xz
 - wget/curl
 - signify (OpenBSD's one)
 - sha512sum

Each release is signed with public key: `RWT4yR0g45MXL1kB6hkxJjeba2+SeC45NnzbeMoyWhtEjeg5bWgum5yQ`.
It is also present in this repo in `sign-sam.pub` file.

Below you can find instructions to install `sam` from binary release
```
VERSION=...
wget https://gitlab.com/scabala/sam/-/releases/${VERSION}/downloads/checksums.txt
wget https://gitlab.com/scabala/sam/-/releases/${VERSION}/downloads/checksums.txt.sig
signify -V -p sign-sam.pub -x checksums.txt.sig -m checksums.txt
wget https://gitlab.com/scabala/sam/-/releases/${VERSION}/downloads/sam-linux-amd64.txz
sha512sum -c --ignore-missing checksums.txt
tar -xf sam-linux-amd64.txz
mv sam ~/.local/bin/
```

## Plugins

`sam` is extensible via plugins which provide way to add new applications to be managed.
Each plugin is Lua script with interface defined as in `example.lua` file. Implement required methods and your good to go.

Sam provides module `sam` with some helper methods like downloading files, unpacking them, checking signatures and so on.
Feel free to use it.

### Discovery

`switch` discovers plugins as Lua scripts in directory specified by `SAM_PLUGIN_ROOT` environment variable.
If it is not set, it falls back to `${XDG_DATA_HOME}/sam/plugins`.  

Name of the plugin is file name without extension.

## Usage

Let say we have file called `foo.lua` in `${XDG_DATA_HOME}/sam/plugins`. We can tell `sam` to use different version of it by:

`sam foo`

It will display terminal menu with all available versions of `foo`. If you hit `q`, program will exit and nothing will happen.
If you choose some version, function `download` from plugin `foo` will be executed. This often mean that it will download specific version of `foo` and will link it to `SAM_LINK_DIR`.

We can also provide version for `foo` to choose from commandline:

`sam -release 1.2.3 foo`

It will not display interactive menu and execute `download` function immediately.

Default log level is `info`. If you want change it, add `-log` flag with proper value like `debug` or `warning`.
