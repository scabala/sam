-- version 1

local sam = require("sam/v1")
local log = require("log")

local pub_key = [[
untrusted comment: sam signing key public key
RWT4yR0g45MXL1kB6hkxJjeba2+SeC45NnzbeMoyWhtEjeg5bWgum5yQ
]]


function versions()
    return sam.gitLabReleases("scabala", "sam")
end

function download(version, dest)
    base_url = "https://example.com/example/" .. version
    base_url = "https://gitlab.com/scabala/sam/-/releases/" .. version .. "/downloads"
    log.info("Downloading checksum signature file")
    sig = sam.fetch(base_url .. "/checksums.txt.sig")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/checksums.txt")
    if not sam.verifySignatureSignify(ch, sig, pub_key)
    then
        error("Checksum file does not match signature")
    end

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/sam-linux-amd64.txz")
    if not sam.checksum(ar, ch, "sha512")
    then
        error("Checksum from file does match calculated checksum")
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "sam", dest .. "sam", "0700")
end

function install(src)
    log.info("Installing sam binary")
    sam.symlink(src .. "sam", os.getenv("HOME") .. "/.local/bin/sam")
end

function uninstall(src)
    sam.unlink(os.getenv("HOME") .. "/.local/bin/sam")
end
