package main

import (
	"errors"
	"io/fs"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

type Plugin interface {
	Name() string
	Validate() (bool, error)
	Download(string, string) error
	Versions() ([]string, error)
	Install(string) error
	Uninstall() error
	Close() error
}

type PluginRegistry struct {
	pluginRoot string
	plugins    map[string]Plugin
}

func (r *PluginRegistry) loadPlugins() error {
	err := filepath.Walk(r.pluginRoot, r.walkCallback)
	if err != nil {
		return err
	}

	return nil
}

func (r *PluginRegistry) register(p Plugin) error {
	valid, err := p.Validate()
	if err != nil {
		return err
	}
	if !valid {
		log.Printf("Plugin %s is not valid", p.Name())
	}

	r.plugins[p.Name()] = p

	return nil
}

func (r *PluginRegistry) walkCallback(p string, info fs.FileInfo, err error) error {
	if err != nil {
		return err
	}

	if p == r.pluginRoot {
		return nil
	}

	if info.IsDir() {
		return nil
	}

	// plugin version 1
	log.Tracef("Processing plugin from path %s", p)
	pv1Supports, err := SupportsPluginV1(p)
	if err != nil {
		log.Errorf("Cannot determine plugin %s. Error: %v", p, err)
		return nil
	}

	if pv1Supports {
		log.Debugf("Plugin %s supports plugin version 1", p)
		pv1, err := NewPluginV1(r.pluginRoot, p)
		if err != nil {
			log.Errorf("Cannot instatiate plugin from %s. Error: %v", p, err)
			return nil
		}
		err = r.register(pv1)
		if err != nil {
			log.Errorf("Cannot register plugin from %s. Error: %v", p, err)
			return nil
		}
		return nil
	}

	// no more plugins protocol versions to check
	log.Debugf("Plugin %s does not support any plugin version", p)

	return nil
}

func (r *PluginRegistry) get(pluginName string) (Plugin, error) {
	plugin, ok := r.plugins[pluginName]
	if !ok {
		return nil, errors.New("Plugin not found")
	}

	return plugin, nil

}

func (r *PluginRegistry) getAll() []Plugin {
	var plugins []Plugin

	for _, p := range r.plugins {
		plugins = append(plugins, p)
	}
	return plugins
}

func (r *PluginRegistry) close() {
	for _, plugin := range r.plugins {
		plugin.Close()
	}
}
