package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name:    "sam",
		Usage:   "simple application manager",
		Action:  rootCmd,
		Version: VersionString,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "log-level",
				Value: "info",
				Usage: "application log level",
			},
			&cli.StringFlag{
				Name:  "format",
				Value: "human",
				Usage: "output format to print",
			},
		},
		Commands: []*cli.Command{
			{
				Name:      "install",
				Usage:     "install specific application release",
				Action:    installCmd,
				ArgsUsage: "<name>",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "release",
						Aliases: []string{"r"},
						Usage:   "release number",
					},
				},
			},
			{
				Name:      "list",
				Usage:     "list all application releases",
				Action:    listCmd,
				ArgsUsage: "<name>",
			},
			{
				Name:      "purge",
				Usage:     "remove ALL installed versions for given application",
				Action:    purgeCmd,
				ArgsUsage: "<name>",
			},
			{
				Name:      "uninstall",
				Usage:     "remove currently installed application",
				Action:    uninstallCmd,
				ArgsUsage: "<name>",
			},
			{
				Name:      "update",
				Usage:     "update application to latest version",
				Action:    updateCmd,
				ArgsUsage: "<name>",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:    "check",
						Aliases: []string{"c"},
						Usage:   "only check for updates",
					},
				},
			},
			{
				Name:      "current",
				Usage:     "show currently used version",
				Action:    currentCmd,
				ArgsUsage: "<name>",
			},
			{
				Name:      "plugin",
				Usage:     "plugin management",
				Action:    func(_ *cli.Context) error { return nil },
				ArgsUsage: "<command>",
				Subcommands: []*cli.Command{
					{
						Name:      "list",
						Usage:     "list all plugins",
						ArgsUsage: "",
						Action:    pluginListCmd,
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Error(err)
		return
	}
}

func rootCmd(c *cli.Context) error {

	log.Debug("Starting SAM - Static Application Manager")
	log.Debug("Closing SAM - Static Application Manager")
	return nil
}

func installCmd(c *cli.Context) error {
	pluginName := c.Args().Get(0)
	release := c.String("release")

	app, err := NewApp(c.String("log-level"))
	if err != nil {
		return err
	}

	err = app.DoInstall(pluginName, release)
	if err != nil {
		return err
	}

	return nil
}

func uninstallCmd(c *cli.Context) error {
	pluginName := c.Args().Get(0)

	app, err := NewApp(c.String("log-level"))
	if err != nil {
		return err
	}

	err = app.DoUninstall(pluginName)
	if err != nil {
		return err
	}

	return nil
}

func purgeCmd(c *cli.Context) error {
	pluginName := c.Args().Get(0)

	app, err := NewApp(c.String("log-level"))
	if err != nil {
		return err
	}

	err = app.DoPurge(pluginName)
	if err != nil {
		return err
	}

	return nil
}

func currentCmd(c *cli.Context) error {
	var pluginNames []string

	app, err := NewApp(c.String("log-level"))
	if err != nil {
		return err
	}

	if c.NArg() == 0 {
		pluginNames = app.DoGetPluginNames()
	} else if c.NArg() == 1 {
		pluginNames = append(pluginNames, c.Args().Get(0))
	} else {
		return errors.New("current command accepts only one argument")
	}

	format := c.String("format")

	currents := make(map[string]string)

	for _, n := range pluginNames {
		current, err := app.DoCurrent(n)
		if err != nil {
			return err
		}
		currents[n] = current

	}

	if format == "human" {
		for name, version := range currents {
			fmt.Printf("%s\t->\t%s\n", name, version)
		}
	} else if format == "json" {
		b, err := json.Marshal(currents)
		if err != nil {
			return err
		}
		os.Stdout.Write(b)
	} else {
		return errors.New("Unknown format")
	}

	return nil
}

func updateCmd(c *cli.Context) error {
	app, err := NewApp(c.String("log-level"))
	if err != nil {
		return err
	}

	check := c.Bool("check")
	format := c.String("format")

	if c.NArg() == 0 {
		if check {
			updates, err := app.DoCheckUpdateAll()
			if err != nil {
				return err
			}
			if format == "human" {
				for k, v := range updates {
					fmt.Printf("%s: available update to %s\n", k, v)
				}
			} else if format == "json" {
				b, err := json.Marshal(updates)
				if err != nil {
					return err
				}
				os.Stdout.Write(b)
			} else {
				return errors.New("Unknown format")
			}
		} else {
			err = app.DoUpdateAll()
			if err != nil {
				return err
			}
		}
	} else if c.NArg() == 1 {
		pluginName := c.Args().Get(0)
		if check {
			update, err := app.DoCheckUpdate(pluginName)
			if err != nil {
				return err
			}
			if update != "" {
				if format == "human" {
					fmt.Printf("%s: available update to %s\n", pluginName, update)
				} else if format == "json" {
					m := make(map[string]string)
					m[pluginName] = update
					b, err := json.Marshal(m)
					if err != nil {
						return err
					}
					os.Stdout.Write(b)
				} else {
					return errors.New("Unknown format")
				}
			}
		} else {
			err = app.DoInstall(pluginName, "latest")
			if err != nil {
				return err
			}
		}
	} else {
		return errors.New("update command accepts only one argument")
	}

	return nil
}

func listCmd(c *cli.Context) error {
	pluginName := c.Args().Get(0)

	app, err := NewApp(c.String("log-level"))
	if err != nil {
		return err
	}

	format := c.String("format")

	versions, err := app.DoGetVersions(pluginName)
	if err != nil {
		return err
	}
	if format == "human" {
		for _, p := range versions {
			fmt.Printf("%s\n", p)
		}
	} else if format == "json" {
		b, err := json.Marshal(versions)
		if err != nil {
			return err
		}
		os.Stdout.Write(b)
	} else {
		return errors.New("Unknown format")
	}

	return nil
}

func pluginListCmd(c *cli.Context) error {
	app, err := NewApp(c.String("log-level"))
	if err != nil {
		return err
	}

	format := c.String("format")

	pluginNames := app.DoGetPluginNames()
	if format == "human" {
		for _, p := range pluginNames {
			fmt.Printf("%s\n", p)
		}
	} else if format == "json" {
		b, err := json.Marshal(pluginNames)
		if err != nil {
			return err
		}
		os.Stdout.Write(b)
	} else {
		return errors.New("Unknown format")
	}

	return nil
}
