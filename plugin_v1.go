package main

import (
	"bufio"
	"os"
	"path/filepath"
	"strings"

	"github.com/yuin/gopher-lua"

	log "github.com/sirupsen/logrus"
)

type PluginV1 struct {
	name    string
	path    string
	lstate  *lua.LState
	module  *SamModuleV1
	tempDir string
}

func SupportsPluginV1(filePath string) (bool, error) {
	if ".lua" != filepath.Ext(filePath) {
		return false, nil
	}

	file, err := os.Open(filePath)
	if err != nil {
		return false, err
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	line, err := reader.ReadString('\n')
	if err != nil {
		return false, err
	}

	if strings.Compare(line, "-- version 1\n") != 0 {
		return false, nil
	}

	return true, nil
}

func NewPluginV1(pluginRoot string, filePath string) (*PluginV1, error) {
	tmp := filePath[len(pluginRoot)+1:] // strip PluginRoot part
	name := tmp[:len(tmp)-4]            // strip '.lua' part

	tempDir, err := os.MkdirTemp("", "sam-plugin-"+strings.Replace(name, "/", "-", -1)+"-*")
	if err != nil {
		return nil, err
	}

	mod := &SamModuleV1{tempDir: tempDir + "/"}

	L := lua.NewState()
	L.PreloadModule("log", logLoader)
	L.PreloadModule("sam/v1", mod.loader)

	err = L.DoFile(filePath)
	if err != nil {
		return nil, err
	}

	return &PluginV1{lstate: L, name: name, path: filePath, module: mod, tempDir: tempDir + "/"}, nil
}

func (p *PluginV1) Name() string {
	return p.name
}

func (p *PluginV1) Versions() ([]string, error) {
	opts := lua.P{
		Fn:      p.lstate.GetGlobal("versions"),
		NRet:    1,
		Protect: true,
	}
	err := p.lstate.CallByParam(opts)
	if err != nil {
		return []string{""}, err
	}
	tab := p.lstate.CheckTable(-1) // returned value
	p.lstate.Pop(1)                // remove received value

	var arr []string
	p.lstate.ForEach(tab, func(_, lvalue lua.LValue) {
		value, ok := lvalue.(lua.LString)
		if !ok {
			// TODO: do something smart
			log.Fatal("some error")
		}
		arr = append(arr, string(value))
	})

	return arr, nil
}

func (p *PluginV1) Download(version string, dest string) error {
	opts := lua.P{
		Fn:      p.lstate.GetGlobal("download"),
		NRet:    0,
		Protect: true,
	}
	err := p.lstate.CallByParam(opts, lua.LString(version), lua.LString(dest))
	if err != nil {
		return err
	}

	return nil
}

func (p *PluginV1) Install(source string) error {
	opts := lua.P{
		Fn:      p.lstate.GetGlobal("install"),
		NRet:    0,
		Protect: true,
	}
	err := p.lstate.CallByParam(opts, lua.LString(source))
	if err != nil {
		return err
	}
	return nil
}

func (p *PluginV1) Uninstall() error {
	opts := lua.P{
		Fn:      p.lstate.GetGlobal("uninstall"),
		NRet:    0,
		Protect: true,
	}
	err := p.lstate.CallByParam(opts)
	if err != nil {
		return err
	}
	return nil
}

func (p *PluginV1) Validate() (bool, error) {
	// TODO: check functions download and versions from plugin, if their signatures are right
	return true, nil
}

func (p *PluginV1) Close() error {
	p.lstate.Close()
	os.RemoveAll(p.tempDir)

	return nil
}
