-- version 1
-- above comment is crucial for plugin to be recognized by SAM

local sam = require("sam/v1")

-- this is example of how sam plugin should look like

-- this is builtin library to unify logging
local log = require("log")

local pub_key = [[
some
multiline
string
like public keys
]]


function versions()
    return sam.gitHubReleases("someone", "somerepo")
end

function download(version)
    base_url = "https://example.com/example/" .. version

    log.info("Downloading checksum signature file")
    sig = sam.fetch(base_url .. "/example_" .. version .. "_SHA256SUMS.sig")

    log.info("Downloading checksum file")
    ch = sam.fetch(base_url .. "/example_" .. version .."_SHA256SUMS")
    if not sam.verifySignaturePGP(ch, sig, pub_key)
    then
        error("Checksum file does not match signature")
    end

    log.info("Downloading release archive")
    ar = sam.fetch(base_url .. "/example_" .. version .. "_linux_amd64.zip")
    if not sam.checksum(ar, ch)
    then
        error("Checksum from file does match calculated checksum")
    end

    log.info("Unpacking binary")
    sam.unpack(ar, "example", sam.tempDir .. "/example", "0700")

    return {sam.tempDir .. "example"}
end

function install(src)
    log.info("Installing example binary")
    sam.symlink(src .. "/example", os.getenv("HOME") .. "/.local/bin")
end
