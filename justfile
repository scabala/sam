sources := `find . -maxdepth 1 -name '*.go' -not -name '*_test.go' -type f -printf '%p '`
version := `git describe --tags --always --dirty`
SIGNIFY_BIN := "signify"

# run target 'sam'
default: sam

# build executable (mainly for development)
sam: fmt
	@go mod tidy
	CGO_ENABLED=0 go build -ldflags "-s -w -X main.VersionString={{version}}" -trimpath -o sam {{sources}}

# build distributable archive for given os and arch
build os arch: fmt deps-updates
	#!/usr/bin/env sh

	if ! test -f dist/sam-{{os}}-{{arch}}.txz
	then
		mkdir -p dist/{{os}}-{{arch}}
		GOOS={{os}} GOARCH={{arch}} CGO_ENABLED=0 go build -ldflags "-s -w -X main.VersionString={{version}}" -o dist/{{os}}-{{arch}}/sam {{sources}}
		cp LICENSE dist/{{os}}-{{arch}}/LICENSE
		tar -cJf dist/sam-{{os}}-{{arch}}.txz -C dist/{{os}}-{{arch}} sam LICENSE
	fi

# create release
release: _release-upload
_release-build: (build "aix" "ppc64") (build "darwin" "amd64") (build "darwin" "arm64") (build "dragonfly" "amd64") (build "freebsd" "386") (build "freebsd" "amd64") (build "freebsd" "arm") (build "freebsd" "arm64") (build "illumos" "amd64") (build "linux" "386") (build "linux" "amd64") (build "linux" "arm") (build "linux" "arm64") (build "linux" "ppc64") (build "linux" "ppc64le") (build "linux" "mips") (build "linux" "mipsle") (build "linux" "mips64") (build "linux" "mips64le") (build "linux" "riscv64") (build "linux" "s390x") (build "netbsd" "386") (build "netbsd" "amd64") (build "netbsd" "arm") (build "openbsd" "386") (build "openbsd" "amd64") (build "openbsd" "arm") (build "openbsd" "arm64") (build "solaris" "amd64")
_release-checksums: _release-build
	if ! test -f dist/checksums.txt; then cd dist && sha512sum *.txz > checksums.txt; fi
_release-sign: _release-checksums
	if ! test -f dist/checksums.txt.sig; then ${SIGNIFY_BIN} -S -s ~/.local/share/sign-sam.sec -m dist/checksums.txt -x dist/checksums.txt.sig; fi
_release-upload: _release-sign
	glab release create --repo scabala/sam $(git describe --tags --abbrev=0) --ref master dist/*.txz dist/checksums.txt dist/checksums.txt.sig

# format source files
fmt:
	gofmt -s -w {{sources}}

# lint source code
lint:
	golangci-lint run

# run security scan
security:
	gosec ./...

# check outdated dependencies
deps-updates:
	@go list -u -m -json all | go-mod-outdated -direct -update

# check security vulnerabilities in dependencies
deps-security:
	@go list  -json -deps | nancy sleuth

# remove artifacts
clean:
	@rm -f sam
	rm -rf dist
