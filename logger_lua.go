package main

import (
	"github.com/yuin/gopher-lua"

	log "github.com/sirupsen/logrus"
)

const messagePrefix = "[PLUGIN] "

func logLoader(L *lua.LState) int {
	mod := L.SetFuncs(L.NewTable(), logMethods)

	L.Push(mod)
	return 1
}

var logMethods = map[string]lua.LGFunction{
	"trace": logTrace,
	"debug": logDebug,
	"info":  logInfo,
	"warn":  logWarn,
	"error": logError,
}

func logTrace(L *lua.LState) int {
	log.Trace("[plugin]")

	return 0
}

func logDebug(L *lua.LState) int {
	message := messagePrefix
	for i := 0; i < L.GetTop(); i++ {
		message = message + L.CheckString(i+1)
	}
	log.Debug(message)

	return 0
}

func logInfo(L *lua.LState) int {
	message := messagePrefix
	for i := 0; i < L.GetTop(); i++ {
		message = message + L.CheckString(i+1)
	}
	log.Info(message)

	return 0
}

func logWarn(L *lua.LState) int {
	message := messagePrefix
	for i := 0; i < L.GetTop(); i++ {
		message = message + L.CheckString(i+1)
	}
	log.Warn(message)

	return 0
}

func logError(L *lua.LState) int {
	message := messagePrefix
	for i := 0; i < L.GetTop(); i++ {
		message = message + L.CheckString(i+1)
	}
	log.Error(message)

	return 0
}
