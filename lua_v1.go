package main

import (
	"github.com/yuin/gopher-lua"
	"runtime"
)

/*
Implements module 'sam' in Lua scripts with helper methods
*/
type SamModuleV1 struct {
	tempDir string
}

func (s *SamModuleV1) loader(L *lua.LState) int {
	t := map[string]lua.LGFunction{
		"menu":                    s.menu,
		"fetch":                   s.fetch,
		"unpack":                  s.unpack,
		"gitHubReleases":          s.gitHubReleases,
		"gitLabReleases":          s.gitLabReleases,
		"checksum":                s.checksum,
		"checksumString":          s.checksumString,
		"verifySignaturePGP":      s.verifySignaturePGP,
		"verifySignatureMinisign": s.verifySignatureMinisign,
		"verifySignatureSignify":  s.verifySignatureSignify,
		"copy":                    s.copy,
		"semverCompare":           s.semverCompare,
		"symlink":                 s.symlink,
		"unlink":                  s.unlink,
		"containerRegistryTags":   s.containerRegistryTags,
		"pypiReleases":            s.pypiReleases,
		"runtimeOs":               s.runtimeOs,
		"runtimeArch":             s.runtimeArch,
	}

	mod := L.SetFuncs(L.NewTable(), t)

	L.SetField(mod, "tempDir", lua.LString(s.tempDir))

	L.Push(mod)
	return 1
}

func (s *SamModuleV1) menu(L *lua.LState) int {
	var idx int
	if L.GetTop() == 2 {
		idx = L.CheckInt(1)
	} else {
		idx = 0

	}

	// construct []string from *lua.LTable
	tab := L.CheckTable(1)
	var arr []string
	L.ForEach(tab, func(lidx, lvalue lua.LValue) {
		idx, err := lidx.(lua.LNumber)
		if err {
			L.RaiseError("%v", err)
		}

		value := lvalue.(lua.LString)
		arr[int(idx)] = string(value)
	})

	ret, err := Menu(arr, idx)
	if err != nil {
		L.RaiseError("%v", err)
	}
	L.Push(lua.LString(ret))
	return 1
}

func (s *SamModuleV1) fetch(L *lua.LState) int {
	dest, err := Fetch(L.CheckString(1), s.tempDir)
	if err != nil {
		L.RaiseError("%v", err)
	}
	L.Push(lua.LString(dest))
	return 1
}

func (s *SamModuleV1) unpack(L *lua.LState) int {
	filepath := L.CheckString(1)
	member := L.CheckString(2)
	target := L.CheckString(3)
	mode := "0600"
	if L.GetTop() == 4 {
		mode = L.CheckString(4)
	}

	err := Unpack(filepath, member, target, mode)
	if err != nil {
		L.RaiseError("%v", err)
	}

	return 0
}

func (s *SamModuleV1) gitHubReleases(L *lua.LState) int {
	username := L.CheckString(1)
	repo := L.CheckString(2)

	var ro ReleaseOptions
	if L.GetTop() == 3 {
		tab := L.CheckTable(3)
		ro.Excludes = make([]string, tab.Len())
		L.ForEach(tab, func(lidx, lvalue lua.LValue) {
			idx, ok := lidx.(lua.LNumber)
			if !ok {
				L.RaiseError("Cannot convert '%v' into lua.LNumber", lidx)
			}
			value := lvalue.(lua.LString)
			ro.Excludes[int(idx)-1] = string(value)
		})
	}

	releases, err := GitHubReleases(username, repo, ro)
	if err != nil {
		L.RaiseError("%v", err)
	}

	// convert []string to *lua.LTable
	tab := L.NewTable()
	for idx, value := range releases {
		tab.Insert(idx, lua.LString(value))
	}

	L.Push(tab)
	return 1
}

func (s *SamModuleV1) gitLabReleases(L *lua.LState) int {
	username := L.CheckString(1)
	repo := L.CheckString(2)

	var ro ReleaseOptions
	if L.GetTop() == 3 {
		tab := L.CheckTable(3)
		ro.Excludes = make([]string, tab.Len())
		L.ForEach(tab, func(lidx, lvalue lua.LValue) {
			idx, ok := lidx.(lua.LNumber)
			if !ok {
				L.RaiseError("Cannot convert '%v' into lua.LNumber", lidx)
			}

			value := lvalue.(lua.LString)
			ro.Excludes[int(idx)-1] = string(value)
		})
	}

	releases, err := GitLabReleases(username, repo, ro)
	if err != nil {
		L.RaiseError("%v", err)
	}

	// convert []string to *lua.LTable
	tab := L.NewTable()
	for idx, value := range releases {
		tab.Insert(idx, lua.LString(value))
	}

	L.Push(tab)
	return 1
}
func (s *SamModuleV1) checksum(L *lua.LState) int {
	filepath := L.CheckString(1)
	checksumPath := L.CheckString(2)
	algorithm := ""
	if L.GetTop() == 3 {
		algorithm = L.CheckString(3)
	}

	valid, err := Checksum(filepath, checksumPath, algorithm)
	if err != nil {
		L.RaiseError("%v", err)
	}

	L.Push(lua.LBool(valid))
	return 1
}

func (s *SamModuleV1) checksumString(L *lua.LState) int {
	filepath := L.CheckString(1)
	checksum := L.CheckString(2)
	algorithm := ""
	if L.GetTop() == 3 {
		algorithm = L.CheckString(3)
	}

	valid, err := ChecksumString(filepath, checksum, algorithm)
	if err != nil {
		L.RaiseError("%v", err)
	}

	L.Push(lua.LBool(valid))

	return 1
}

func (s *SamModuleV1) verifySignaturePGP(L *lua.LState) int {
	dataPath := L.CheckString(1)
	signaturePath := L.CheckString(2)
	key := L.CheckString(3)

	var signatureArmored bool
	if L.GetTop() == 4 {
		signatureArmored = L.CheckBool(4)
	} else {
		signatureArmored = false
	}

	valid, err := VerifySignaturePGP(dataPath, signaturePath, []byte(key), signatureArmored)
	if err != nil {
		L.RaiseError("%v", err)
	}
	L.Push(lua.LBool(valid))

	return 1
}

func (s *SamModuleV1) verifySignatureSignify(L *lua.LState) int {
	dataPath := L.CheckString(1)
	signaturePath := L.CheckString(2)
	key := L.CheckString(3)

	valid, err := VerifySignatureSignify(dataPath, signaturePath, []byte(key))
	if err != nil {
		L.RaiseError("%v", err)
	}
	L.Push(lua.LBool(valid))

	return 1
}

func (s *SamModuleV1) verifySignatureMinisign(L *lua.LState) int {
	dataPath := L.CheckString(1)
	signaturePath := L.CheckString(2)
	key := L.CheckString(3)

	valid, err := VerifySignatureMinisign(dataPath, signaturePath, []byte(key))
	if err != nil {
		L.RaiseError("%v", err)
	}
	L.Push(lua.LBool(valid))

	return 1
}

func (s *SamModuleV1) copy(L *lua.LState) int {
	srcPath := L.CheckString(1)
	dstPath := L.CheckString(2)
	mode := "0600"
	if L.GetTop() == 3 {
		mode = L.CheckString(3)
	}

	err := Copy(srcPath, dstPath, mode)
	if err != nil {
		L.RaiseError("%v", err)
	}
	return 0
}

func (s *SamModuleV1) semverCompare(L *lua.LState) int {
	version := L.CheckString(1)
	constraint := L.CheckString(2)
	ret, err := SemVerCompare(version, constraint)
	if err != nil {
		L.RaiseError("%v", err)
	}
	L.Push(lua.LBool(ret))
	return 1
}

func (s *SamModuleV1) symlink(L *lua.LState) int {
	srcPath := L.CheckString(1)
	dstPath := L.CheckString(2)
	mode := "0600"
	if L.GetTop() == 3 {
		mode = L.CheckString(3)
	}

	err := Symlink(srcPath, dstPath, mode)
	if err != nil {
		L.RaiseError("%v", err)
	}
	return 0
}

func (s *SamModuleV1) unlink(L *lua.LState) int {
	path := L.CheckString(1)

	err := Unlink(path)
	if err != nil {
		L.RaiseError("%v", err)
	}
	return 0
}

func (s *SamModuleV1) containerRegistryTags(L *lua.LState) int {
	image := L.CheckString(1)

	var ro ReleaseOptions
	if L.GetTop() == 3 {
		tab := L.CheckTable(3)
		ro.Excludes = make([]string, tab.Len())
		L.ForEach(tab, func(lidx, lvalue lua.LValue) {
			idx, ok := lidx.(lua.LNumber)
			if !ok {
				L.RaiseError("Cannot convert '%v' into lua.LNumber", lidx)
			}
			value := lvalue.(lua.LString)
			ro.Excludes[int(idx)-1] = string(value)
		})
	}

	tags, err := ContainerRegistryTags(image, ro)
	if err != nil {
		L.RaiseError("%v", err)
	}

	// convert []string to *lua.LTable
	tab := L.NewTable()
	for idx, value := range tags {
		tab.Insert(idx, lua.LString(value))
	}

	L.Push(tab)
	return 1
}

func (s *SamModuleV1) pypiReleases(L *lua.LState) int {
	pkg := L.CheckString(1)

	var ro ReleaseOptions
	if L.GetTop() == 3 {
		tab := L.CheckTable(3)
		ro.Excludes = make([]string, tab.Len())
		L.ForEach(tab, func(lidx, lvalue lua.LValue) {
			idx, ok := lidx.(lua.LNumber)
			if !ok {
				L.RaiseError("Cannot convert '%v' into lua.LNumber", lidx)
			}
			value := lvalue.(lua.LString)
			ro.Excludes[int(idx)-1] = string(value)
		})
	}

	tags, err := PypiReleases(pkg, ro)
	if err != nil {
		L.RaiseError("%v", err)
	}

	// convert []string to *lua.LTable
	tab := L.NewTable()
	for idx, value := range tags {
		tab.Insert(idx, lua.LString(value))
	}

	L.Push(tab)
	return 1
}

func (s *SamModuleV1) runtimeOs(L *lua.LState) int {
	L.Push(lua.LString(runtime.GOOS))
	return 1
}

func (s *SamModuleV1) runtimeArch(L *lua.LState) int {
	L.Push(lua.LString(runtime.GOARCH))
	return 1
}
