package main

import (
	"encoding/json"
	"errors"
	"os"
	"path"
	"time"

	log "github.com/sirupsen/logrus"
)

type Cache struct {
	path string
	ttl  int
}

type cacheEntry struct {
	Value []string
	Stamp string
}

var notFoundErr error = errors.New("Key not found in cache")
var ttlExceededErr error = errors.New("Key is too old")

func NewCache(cachePath string, ttl int) (*Cache, error) {
	log.Tracef("DiskCache path: %s", cachePath)

	c := Cache{path: cachePath, ttl: ttl}

	_, err := os.ReadFile(c.path)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(path.Dir(c.path), 0700)
			if err != nil {
				return nil, err
			}
			err = os.WriteFile(c.path, []byte("{}"), os.ModePerm)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}
	return &c, nil
}

func (c *Cache) Get(key string) ([]string, error) {
	var ret []string

	data, err := os.ReadFile(c.path)
	if err != nil {
		return ret, err
	}

	var entries map[string]cacheEntry
	err = json.Unmarshal(data, &entries)
	if err != nil {
		return ret, err
	}

	entry, ok := entries[key]
	if !ok {
		return ret, notFoundErr
	}

	if entry.Stamp == "" {
		return ret, notFoundErr
	}

	stamp, err := time.Parse(time.RFC3339, entry.Stamp)
	if err != nil {
		return ret, err
	}

	t := time.Now().Add(-time.Hour * 1)
	if t.After(stamp) { // invalidate cache
		return ret, ttlExceededErr
	}

	ret = entry.Value

	return ret, nil
}

func (c *Cache) Set(key string, value []string) error {
	data, err := os.ReadFile(c.path)
	if err != nil {
		return err
	}

	var entries map[string]cacheEntry
	err = json.Unmarshal(data, &entries)
	if err != nil {
		return err
	}

	entries[key] = cacheEntry{Value: value, Stamp: time.Now().Format(time.RFC3339)}

	data2, err := json.Marshal(entries)
	if err != nil {
		return err
	}

	err = os.WriteFile(c.path, data2, 0600)
	if err != nil {
		return err
	}

	return nil
}
